FROM python:3.8-slim

ARG RADICALE_VERSION
ENV RADICALE_VERSION 3.0.6

ARG BUILD_UID
ENV BUILD_UID ${BUILD_UID:-2999}

ARG BUILD_GID
ENV BUILD_GID ${BUILD_GID:-2999}

EXPOSE 5232

RUN addgroup --gid $BUILD_GID radicale \ 
    && adduser --system --shell /bin/false --no-create-home --uid $BUILD_UID radicale \
    && adduser radicale radicale \
    && mkdir -p /config /data \
    && pip install radicale==${RADICALE_VERSION} passlib[bcrypt] \
    && chmod -R 770 /data \
    && chown -R radicale:radicale /data

COPY config /config/config
USER radicale

ENTRYPOINT ["radicale"]
CMD ["--config", "/config/config"]
