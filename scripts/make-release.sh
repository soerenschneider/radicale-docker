#!/bin/sh

set -e

AUTOMATION=${1:-no}

set -u

get_latest_official_release() {
    curl -s https://pypi.org/pypi/Radicale/json | \
        jq -j ".releases | keys" | \
        tr -d '"' | \
        sort -nr | \
        head -n1 | \
        xargs
}

get_latest_local_release() {
    git tag | sort -nr | head -n1
}

make_release() {
    VERSION=${OFFICIAL} envsubst '${VERSION}' < Dockerfile.tmpl > Dockerfile
	git add Dockerfile
	git commit -m "Updated radicale to ${OFFICIAL}"
	git tag ${OFFICIAL}
	git push
	git push --tags
}

OFFICIAL="$(get_latest_official_release)"
LOCAL="$(get_latest_local_release)"

if [ "${OFFICIAL}" = "${LOCAL}" ]; then
    echo "No updates, both versions at ${OFFICIAL}"
    exit 0
fi

if [ "${AUTOMATION}" != "yes" ]; then
    while true; do
        read -p "Build release for radicale ${OFFICIAL}? " answer
        case $answer in
            [Yy]* ) make_release; break;;
            [Nn]* ) exit 0;;
            * )     echo "Please answer yes or no.";;
        esac
    done
elif [ "${AUTOMATION}" = "yes" ];  then
    make_release
fi
